package site.sorghum.feignless.solon.configuration;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.solon.FeignClient;
import feign.solon.FeignConfiguration;

/**
 * 默认JackSon配置
 *
 * @author Sorghum
 * @since 2023/01/11
 */
public class JacksonConfig implements FeignConfiguration {
    @Override
    public Feign.Builder config(FeignClient client, Feign.Builder builder) {
        return builder.encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder());
    }
}