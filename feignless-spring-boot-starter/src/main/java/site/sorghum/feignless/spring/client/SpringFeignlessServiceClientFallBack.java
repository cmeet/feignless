package site.sorghum.feignless.spring.client;

import site.sorghum.feignless.pojo.RemoteCommunicateData;

/**
 * 快装客户端回调
 *
 * @author Sorghum
 * @since 2022/12/15
 */
public class SpringFeignlessServiceClientFallBack implements SpringFeignlessServiceClient {
    @Override
    public RemoteCommunicateData invoke(RemoteCommunicateData remoteCommunicateData) {
        return RemoteCommunicateData.EMPTY;
    }
}
