# Feignless

#### 介绍

一个不需要创建FeignClient的Feign拓展框架，类似于Dubbo般的优雅调用。

你还在为使用Feign时，频繁创建Controller，频繁维护FeignClient而烦恼吗？
快来使用FeignLess吧，支持LoadBalance，FallBack，Retryer，TimeOut等部分原生特性。

#### 软件架构

基于Feign，支持LoadBalance等部分原生特性。
已适配：Solon Cloud ，Spring Cloud OpenFeign。
#### 使用说明
> 当前版本：revision 1.1.4
1. 服务端
    1. 引入依赖
        1. ```xml
           <!--spring-->
           <dependency>
                <groupId>site.sorghum.feignless</groupId>
                <artifactId>feignless-spring-boot-starter</artifactId>
               <version>${revision}</version>
           </dependency>
           <!--solon-->
            <dependency>
                <groupId>site.sorghum.feignless</groupId>
                <artifactId>feignless-solon-plugin</artifactId>
               <version>${revision}</version>
           </dependency>
           ```
    2. 在服务端的启动类上添加注解`@EnableFeignless(scanBasePackages = "site.sorghum.feignless.demo")`
    3. 配置文件配置通信加密密钥,sm4密钥，可自行生成，长度为：16
        1. ```yaml
           feignless:
               private-key: 123456781234123456
           ```
2. 客户端
    1. 引入依赖
        1. ```xml
            <!--spring-->
           <dependency>
                <groupId>site.sorghum.feignless</groupId>
                <artifactId>feignless-spring-boot-starter</artifactId>
               <version>${revision}</version>
           </dependency>
           <!--solon-->
            <dependency>
                <groupId>site.sorghum.feignless</groupId>
                <artifactId>feignless-solon-plugin</artifactId>
               <version>${revision}</version>
           </dependency>
           ```
    2. 在客户端的启动类上添加注解`@EnableFeignless`
    3. 配置文件配置通信加密密钥,aes密钥，可自行生成
       1. ```yaml
           feignless:
               private-key: 123456781234123456
           ```
    4. 在需要调用的Service接口上添加注解`@FeignlessClient`
       1. `@FeignlessClient`的`serviceName`属性为服务端的应用名,即Feign服务名
       2. `@FeignlessClient`的`targetBeanName`属性为远程调用的对应的Bean名，即FeignClient的Bean名
       3. `@FeignlessClient`的`serviceUrl`指定URL，则serviceName失效，直连不走负载均衡。
       4. 额外配置，详见代码，支持fallback，feign原生超时，原生重试机制。
          1. fallback中支持注入处理过的异常。
          2. ```java
             @Setter
             Throwable cause;
             ```
       5. ```java 
                @FeignlessClient(serviceName = "demo-server", solonGroup = "demo",targetBeanName = "demoService", serviceUrl = "http://localhost:8080")
                public interface DemoService {
                    
                }
                // 一般来说，基于负载均衡的调用，不需要指定serviceUrl，
                // Solon 以下配置即可  solonGroup：nacos的group分区名
                @FeignlessClient(serviceName = "demo-server", solonGroup = "demo",targetBeanName = "demoService")
                public interface DemoService {
                    
                }
                // Spring 以下配置即可
                @FeignlessClient(serviceName = "demo-server",targetBeanName = "demoService")
                public interface DemoService {
                    
                }
           ```
    5. 注入接口对应的Bean即可使用
       1. ```java
                // Spring
                @InjectFeignBean(targetBeanName = "testFeignClient",targetBeanType=UserService.class)
                private UserService userService;
                // Solon
                @Inject
                private UserService userService;
           ```
       2. ```java
                // Spring
                UserService userService = SpringUtil.getBean(UserService.class);
                // Solon
                UserService userService = Solon.context().getBean(UserService.class);
          ```