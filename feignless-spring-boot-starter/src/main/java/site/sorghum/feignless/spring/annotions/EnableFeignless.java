package site.sorghum.feignless.spring.annotions;

import org.springframework.context.annotation.Import;
import site.sorghum.feignless.spring.configuration.FeignlessConfiguration;

import java.lang.annotation.*;

/**
 * 使feignless
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(FeignlessConfiguration.class)
public @interface EnableFeignless {

    /**
     * 扫描的包
     *
     * @return {@link String[]}
     */
    String[] basePackages() default {};
}
