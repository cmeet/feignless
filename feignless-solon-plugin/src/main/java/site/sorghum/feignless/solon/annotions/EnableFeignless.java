package site.sorghum.feignless.solon.annotions;

import java.lang.annotation.*;

/**
 * 使feignless
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface EnableFeignless {

    /**
     * 扫描的包
     *
     * @return {@link String[]}
     */
    String[] basePackages() default {};
}
