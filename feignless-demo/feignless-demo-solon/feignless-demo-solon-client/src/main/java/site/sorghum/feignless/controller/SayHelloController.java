package site.sorghum.feignless.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import site.sorghum.feignless.service.DemoService;

/**
 * 说hello控制器
 *
 * @author Sorghum
 * @since 2023/01/12
 */
@Controller
@Mapping("/sayHello")
public class SayHelloController {

    @Inject
    DemoService demoService;

    @Mapping
    public String sayHello(){
        return demoService.hello("World");
    }
}
