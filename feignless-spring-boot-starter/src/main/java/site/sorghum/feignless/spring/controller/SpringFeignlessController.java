package site.sorghum.feignless.spring.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.sorghum.feignless.pojo.RemoteCommunicateData;
import site.sorghum.feignless.server.FeignlessController;

/**
 * 快装控制器
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@RestController
public class SpringFeignlessController extends FeignlessController {

    @PostMapping("/extend/feignless/invoke/")
    @Override
    public RemoteCommunicateData invoke(@RequestBody RemoteCommunicateData remoteCommunicateData) {
        return super.invoke(remoteCommunicateData);
    }


}
