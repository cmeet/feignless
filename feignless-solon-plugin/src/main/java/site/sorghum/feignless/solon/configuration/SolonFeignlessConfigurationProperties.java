package site.sorghum.feignless.solon.configuration;

import site.sorghum.feignless.configuration.FeignlessConfigurationProperties;

/**
 * feignless配置属性
 *
 * @author Sorghum
 * @since 2022/12/16
 */
public class SolonFeignlessConfigurationProperties extends FeignlessConfigurationProperties {
}
