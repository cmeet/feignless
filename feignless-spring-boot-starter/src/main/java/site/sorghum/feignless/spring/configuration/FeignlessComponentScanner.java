package site.sorghum.feignless.spring.configuration;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.log.StaticLog;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import site.sorghum.feignless.annotions.InjectFeignBean;
import site.sorghum.feignless.utils.Util;

import java.lang.reflect.Field;

/**
 * 组件扫描
 * Feign Bean 注入
 *
 * @author Sorghum
 * @since 2022/12/20
 */
@Configuration
public class FeignlessComponentScanner implements InstantiationAwareBeanPostProcessor {

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        boolean isFastAsync = Util.isInjectClazz(bean);
        if (isFastAsync) {
            Class<?> targetClass = Util.targetClass(bean);
            Field[] fields = ReflectUtil.getFields(targetClass, field -> AnnotationUtil.getAnnotation(field, InjectFeignBean.class) != null);
            for (Field field : fields) {
                InjectFeignBean asyncInject = field.getAnnotation(InjectFeignBean.class);
                // Async Inject Bean
                FeignlessRunnableQueue.addRunnable(() -> {
                    try {
                        String value = asyncInject.targetBeanName();
                        Class<?> beanClazz = asyncInject.targetBeanType().equals(Object.class) ? field.getType() : asyncInject.targetBeanType();
                        Object tg = null;
                        if (StrUtil.isBlank(value)) {
                            tg = SpringUtil.getBean(beanClazz);
                        }else {
                            tg = SpringUtil.getBean(value);
                        }
                        ReflectUtil.setFieldValue(bean, field, tg);
                    }catch (Exception e){
                        StaticLog.error(e);
                    }
                });
            }
        }
        return true;
    }

}
