package site.sorghum.feignless;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import site.sorghum.feignless.spring.annotions.EnableFeignless;

/**
 * 装服务器应用程序
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@SpringBootApplication
@EnableFeignless(basePackages = "site.sorghum")
@EnableFeignClients
@EnableDiscoveryClient
public class SpringFeignServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringFeignServerApplication.class, args);
    }
}