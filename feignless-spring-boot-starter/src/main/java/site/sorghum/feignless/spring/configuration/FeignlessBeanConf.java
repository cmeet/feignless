package site.sorghum.feignless.spring.configuration;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.FIFOCache;
import cn.hutool.extra.spring.SpringUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import site.sorghum.feignless.configuration.FeignlessConfigurationProperties;
import site.sorghum.feignless.utils.ContextUtil;
import site.sorghum.feignless.utils.Util;

/**
 * feignless bean 配置
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@Configuration
public class FeignlessBeanConf {

    @Bean
    @ConditionalOnMissingBean(name = "feignlessCache")
    FIFOCache<String ,String> feignlessCache(){
        return CacheUtil.newFIFOCache(3000);
    }

    @Bean
    ContextUtil contextUtil(){
        return new ContextUtil() {
            @Override
            public <T> T getBean(String name) {
                return SpringUtil.getBean(name);
            }

            @Override
            public <T> T getBean(Class<T> clazz) {
                return SpringUtil.getBean(clazz);
            }

            @Override
            public <T> T getBean(String name, Class<T> clazz) {
                return SpringUtil.getBean(name,clazz);
            }
        };
    }

    @Bean
    Util util(FeignlessConfigurationProperties feignlessConfigurationProperties, FIFOCache<String, String> feignlessFifoCache, ContextUtil contextUtil) {
        return new Util(feignlessConfigurationProperties, feignlessFifoCache, contextUtil);
    }
}
