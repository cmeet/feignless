package site.sorghum.feignless.configuration;

import lombok.Data;

/**
 * feignless配置属性
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@Data
public class FeignlessConfigurationProperties{
    /**
     * 通信私钥
     */
    String privateKey;
}
