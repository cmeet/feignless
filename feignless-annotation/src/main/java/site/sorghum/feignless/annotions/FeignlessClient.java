package site.sorghum.feignless.annotions;



import java.lang.annotation.*;


/**
 * @author Sorghum
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface FeignlessClient {

    /**
     * 所在Feign的服务名称
     *
     * @return 服务名称
     */
    String serviceName();


    /**
     * 所在Feign的服务直链
     *
     * @return 服务直链
     */
    String serviceUrl() default "";

    /**
     * Hook的Bean名称
     *
     * @return {@link String}
     */
    String targetBeanName() default "";

    /**
     * 目标类名字
     *
     * @return {@link String}
     */
    String targetClassName() default "";

    /**
     * 毫秒
     * Feign Request.Options 参数 connectTimeout 连接超时,参考 {@link Request.Options#connectTimeout()}
     *
     * @return long
     */
    long connectTimeout() default -1L;

    /**
     * 毫秒
     * Feign Request.Options 参数 readTimeout 读取超时,参考 {@link feign.Request.Options#readTimeout()}
     *
     * @return long
     */
    long readTimeout() default -1L;

    /**
     * Feign Retryer period参数,参考{@link feign.Retryer.Default}
     *
     * @return long
     */
    long period() default -1L;

    /**
     * Feign Retryer maxPeriod参数,参考{@link feign.Retryer.Default}
     *
     * @return long
     */
    long maxPeriod() default -1L;

    /**
     * Feign Retryer maxAttempts参数,参考{@link feign.Retryer.Default}
     *
     * @return long
     */
    long maxAttempts() default -1L;

    /**
     * 回退Class
     *
     * @return {@link Class}<{@link ?}>
     */
    Class<?> fallback() default void.class;

    /**
     * Solon App Group 参数
     *
     * @return group分组
     */
    String solonGroup() default "";

    /**
     * 配置类：Solon Class<? extends FeignConfiguration> configuration() default FeignConfiguration.class;
     *
     * @return {@link Class}<{@link ?}>
     */
    Class<?> solonConfiguration() default void.class;

}
