package site.sorghum.feignless;

import cn.hutool.core.util.RandomUtil;
import org.noear.solon.aspect.annotation.Service;
import site.sorghum.feignless.service.DemoService;

/**
 * 演示服务impl
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@Service(name = "demoService")
public class DemoServiceImpl implements DemoService {

    @Override
    public String hello(String name) {
        if (RandomUtil.randomBoolean()){
            throw new RuntimeException("随机异常");
        }
        return "Hello " + name;
    }

    @Override
    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }

}
