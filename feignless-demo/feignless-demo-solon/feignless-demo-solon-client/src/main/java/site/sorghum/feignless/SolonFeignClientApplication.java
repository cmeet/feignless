package site.sorghum.feignless;

import cn.hutool.core.lang.Console;
import cn.hutool.core.thread.ConcurrencyTester;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.log.StaticLog;
import feign.solon.EnableFeignClient;
import org.noear.solon.Solon;
import site.sorghum.feignless.service.DemoService;
import site.sorghum.feignless.solon.annotions.EnableFeignless;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 装服务器应用程序
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@EnableFeignless(basePackages = "site.sorghum")
@EnableFeignClient
public class SolonFeignClientApplication {
    public static void main(String[] args) {
        AtomicLong sumLong = new AtomicLong();
        AtomicLong errLong = new AtomicLong();
        Solon.start(SolonFeignClientApplication.class, args);
        DemoService bean = Solon.context().getBean(DemoService.class);
        ConcurrencyTester tester = ThreadUtil.concurrencyTest(20, () -> {
            int cnt = 200;
            while (true) {
                if (cnt-- == 0) {
                    break;
                }
                sumLong.incrementAndGet();
                try {
                    // 测试的逻辑内容
                    String word = bean.hello("Word");
                    StaticLog.info(word);
                } catch (Exception e) {
                    e.printStackTrace();
                    StaticLog.error("测试失败,{}",errLong.incrementAndGet());
                    break;
                }
            }
        });
        Console.log("总共执行了{}次", sumLong.get());
        // 获取总的执行时间，单位毫秒
        Console.log("总执行时长：{}",tester.getInterval());
        // 获取每毫秒执行次数
        Console.log("每毫秒执行次数：{}",sumLong.get()/tester.getInterval());
        // 错误次数
        Console.log("错误次数：{}",errLong.get());
        // 错误的百分比
        Console.log("错误百分比：{}%",errLong.get()*100/sumLong.get());
    }
}