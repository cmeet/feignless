package site.sorghum.feignless.spring.client;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import site.sorghum.feignless.client.FeignlessServiceClient;
import site.sorghum.feignless.pojo.RemoteCommunicateData;

/**
 * 快装客户端
 *
 * @author Sorghum
 * @since 2022/12/15
 */
public interface SpringFeignlessServiceClient extends FeignlessServiceClient {

    /**
     * 调用
     *
     * @param remoteCommunicateData 远程通信数据
     * @return {@link RemoteCommunicateData}
     */
    @PostMapping("/extend/feignless/invoke/")
    @Override
    RemoteCommunicateData invoke(@RequestBody RemoteCommunicateData remoteCommunicateData);
}
