package site.sorghum.feignless.solon;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.ClassLoaderUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.log.StaticLog;
import feign.Feign;
import feign.Request;
import feign.Retryer;
import feign.solon.FeignClient;
import feign.solon.FeignConfiguration;
import feign.solon.integration.FeignTarget;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import org.noear.solon.Solon;
import org.noear.solon.Utils;
import org.noear.solon.core.*;
import site.sorghum.feignless.annotions.FeignlessClient;
import site.sorghum.feignless.client.aop.AopInterceptor;
import site.sorghum.feignless.solon.annotions.EnableFeignless;
import site.sorghum.feignless.solon.client.SolonFeignlessServiceClient;
import site.sorghum.feignless.solon.client.SolonFeignlessServiceClientFallBack;
import site.sorghum.feignless.solon.configuration.JacksonConfig;
import site.sorghum.feignless.utils.Util;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * FeignLess插件
 *
 * @author Sorghum
 * @since 2023/01/10
 */
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
public class XPluginImp implements Plugin {

    @Override
    public void start(AopContext context) throws Throwable {
        //检查是否启用了@FeignClient
        EnableFeignless enableFeignless = Solon.app().source().getAnnotation(EnableFeignless.class);
        if (enableFeignless == null) {
            return;
        }
        // 主动扫描
        context.beanScan("site.sorghum.feignless");

        Set<Class<?>> classes = new HashSet<>();
        Arrays.stream(enableFeignless.basePackages()).sequential().forEach(basePackage -> {
            classes.addAll(ClassUtil.scanPackageByAnnotation(basePackage, FeignlessClient.class));
        });
        // 生成通用服务的FeignClient代理类
        classes.stream().map(clazz -> AnnotationUtil.getAnnotation(clazz, FeignlessClient.class)).forEach(feignlessClient -> {
            String name = feignlessClient.serviceName();
            String serviceUrl = feignlessClient.serviceUrl();
            String solonGroup = feignlessClient.solonGroup();
            Class<?> configuration = feignlessClient.solonConfiguration().equals(void.class) ? JacksonConfig.class : feignlessClient.solonConfiguration();
            // 如果本服务名与FeignClient的服务名相同，则不生成代理类
            if (name.equals(getSelfServiceName()) && solonGroup.equals(getSelfServiceGroup())) {
                return;
            }
            StaticLog.info("生成Feign服务代理实例，服务名：:{}", name);
            FeignClient feignClient = createFeignClient(serviceUrl, solonGroup, name, "", SolonFeignlessServiceClientFallBack.class, (Class<? extends FeignConfiguration>) configuration);
            getProxy(context, SolonFeignlessServiceClient.class, feignClient, builder -> {
                        if (feignlessClient.connectTimeout() > 0) {
                            Request.Options options = new Request.Options((int) feignlessClient.connectTimeout(), (int) feignlessClient.readTimeout());
                            builder.options(options);
                        }
                        if (feignlessClient.period() > 0) {
                            Retryer retryer = new Retryer.Default(feignlessClient.period(),
                                    feignlessClient.maxPeriod(),
                                    (int) feignlessClient.maxAttempts());
                            builder.retryer(retryer);
                        }
                    }, obj ->
                    {
                        String feignBeanName = Util.feignlessClientAnnotationName(feignlessClient);
                        BeanWrap wrap = context.wrap(feignBeanName, obj);
                        context.putWrap(feignBeanName, wrap);
                    }
            );
        });
        // 生成特定服务的FeignClient代理类
        classes.forEach(clazz -> {
            FeignlessClient feignlessClient = AnnotationUtil.getAnnotation(clazz, FeignlessClient.class);
            // 如果本服务名与FeignClient的服务名相同，则不生成代理类
            if (feignlessClient.serviceName().equals(getSelfServiceName()) && feignlessClient.solonGroup().equals(getSelfServiceGroup())) {
                return;
            }
            DynamicType.Unloaded<?> dynamicType = new ByteBuddy().subclass(clazz)
                    .name(clazz.getName() + "$PROXY")
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.any())).intercept(FixedValue.nullValue())
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(short.class))).intercept(FixedValue.value((short) 0))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(int.class))).intercept(FixedValue.value(0))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(char.class))).intercept(FixedValue.value('0'))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(byte.class))).intercept(FixedValue.value((byte) 0))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(float.class))).intercept(FixedValue.value(0.0F))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(double.class))).intercept(FixedValue.value(0.0D))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(long.class))).intercept(FixedValue.value(0L))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(boolean.class))).intercept(FixedValue.value(false))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(TypeDescription.VOID))).intercept(FixedValue.value(TypeDescription.VOID))
                    //AOP 代理 环绕
                    .method(ElementMatchers.isDeclaredBy(clazz))
                    .intercept(MethodDelegation.to(AopInterceptor.class))
                    .make();
            Class<?> aClass = dynamicType.load(ClassLoaderUtil.getContextClassLoader())
                    .getLoaded();
            try {
                Object newInstance = aClass.getDeclaredConstructor().newInstance();
                BeanWrap wrap = context.wrap(feignlessClient.targetBeanName(), newInstance);
                context.putWrap(clazz, wrap);
                StaticLog.info("生成目标服务为：{}，类为：{} 的服务远程代理Bean", feignlessClient.serviceName(), aClass.getName());
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                     InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        });
    }


    @Override
    public void prestop() throws Throwable {
        Plugin.super.prestop();
    }

    @Override
    public void stop() throws Throwable {
        Plugin.super.stop();
    }


    private void getProxy(AopContext ctx, Class<?> clz, FeignClient anno, Consumer<Feign.Builder> builderConsumer, Consumer consumer) {
        //获取配置器
        FeignConfiguration configuration = ctx.wrapAndPut(anno.configuration()).get();

        //生成构建器
        Feign.Builder builder0 = Feign.builder();

        //初始化构建器
        builder0.options(new Request.Options(1000, 3500))
                .retryer(new Retryer.Default(5000, 5000, 3));

        //进行配置
        builder0 = configuration.config(anno, builder0);

        Feign.Builder builder = builder0;
        builderConsumer.accept(builder);
        //构建target
        if (Utils.isEmpty(anno.url())) {
            LoadBalance upstream = getUpstream(anno);
            if (upstream != null) {
                FeignTarget target = new FeignTarget(clz, anno.name(), anno.path(), upstream);
                consumer.accept(builder.target(target));
            } else {
                ctx.getWrapAsync(anno.name(), (bw) -> {
                    LoadBalance tmp = bw.raw();
                    FeignTarget target = new FeignTarget(clz, anno.name(), anno.path(), tmp);
                    consumer.accept(builder.target(target));
                });
            }
        } else {
            FeignTarget target = new FeignTarget(clz, anno.name(), anno.path(), () -> anno.url());
            consumer.accept(builder.target(target));
        }
    }

    private String getSelfServiceName() {
        return Solon.context().cfg().get("solon.app.name");
    }

    private String getSelfServiceGroup() {
        return Solon.context().cfg().get("solon.app.group");
    }

    private LoadBalance getUpstream(FeignClient anno) {
        if (Bridge.upstreamFactory() == null) {
            return null;
        }

        return Bridge.upstreamFactory().create(anno.group(), anno.name());
    }

    private FeignClient createFeignClient(String url, String group, String name, String path, Class<?> fallback, Class<? extends FeignConfiguration> configuration) {
        return new FeignClient() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return FeignClient.class;
            }

            @Override
            public String url() {
                return url;
            }

            @Override
            public String group() {
                return group;
            }

            @Override
            public String name() {
                return name;
            }

            @Override
            public String path() {
                return path;
            }

            @Override
            public Class<? extends FeignConfiguration> configuration() {
                return configuration;
            }

            @Override
            public Class<?> fallback() {
                return fallback;
            }

            @Override
            public Class<?> fallbackFactory() {
                return void.class;
            }

            {
            }
        };
    }

    @Override
    public void init(AopContext context) throws Throwable {
        Plugin.super.init(context);
    }
}
