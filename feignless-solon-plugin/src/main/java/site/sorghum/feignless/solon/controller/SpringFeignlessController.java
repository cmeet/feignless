package site.sorghum.feignless.solon.controller;

import org.noear.solon.annotation.Body;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.MethodType;
import site.sorghum.feignless.pojo.RemoteCommunicateData;
import site.sorghum.feignless.server.FeignlessController;

/**
 * 快装控制器
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@Controller
public class SpringFeignlessController extends FeignlessController {

    @Mapping(value = "/extend/feignless/invoke/",method = MethodType.POST)
    @Override
    public RemoteCommunicateData invoke(@Body RemoteCommunicateData remoteCommunicateData) {
        return super.invoke(remoteCommunicateData);
    }


}
