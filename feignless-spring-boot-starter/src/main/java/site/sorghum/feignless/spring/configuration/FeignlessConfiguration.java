package site.sorghum.feignless.spring.configuration;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.ClassLoaderUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.log.StaticLog;
import feign.Request;
import feign.Retryer;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.cloud.openfeign.FeignClientBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import site.sorghum.feignless.annotions.FeignlessClient;
import site.sorghum.feignless.client.aop.AopInterceptor;
import site.sorghum.feignless.spring.annotions.EnableFeignless;
import site.sorghum.feignless.spring.client.SpringFeignlessServiceClient;
import site.sorghum.feignless.spring.client.SpringFeignlessServiceClientFallBack;
import site.sorghum.feignless.utils.Util;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 快装配置
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@Configuration
public class FeignlessConfiguration implements ImportBeanDefinitionRegistrar, InitializingBean {

    private static AnnotationMetadata metadata;

    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry, BeanNameGenerator importBeanNameGenerator) {
        // 获取注解属性
        FeignlessConfiguration.metadata = metadata;
    }

    @Override
    public void afterPropertiesSet() {
        if (!enableFeignless()) {
            return;
        }
        StaticLog.info("FeignlessConfiguration init Start");
        ApplicationContext applicationContext = SpringUtil.getApplicationContext();
        FeignClientBuilder clientBuilder = new FeignClientBuilder(applicationContext);
        Set<Class<?>> classes = new HashSet<>();
        Arrays.stream(getBasePackage(metadata)).sequential().forEach(basePackage -> {
            classes.addAll(ClassUtil.scanPackageByAnnotation(basePackage, FeignlessClient.class));
        });
        // 生成通用服务的FeignClient代理类
        classes.stream().map(clazz -> AnnotationUtil.getAnnotation(clazz, FeignlessClient.class)).forEach(feignlessClient -> {
            String name = feignlessClient.serviceName();
            String serviceUrl = feignlessClient.serviceUrl();
            // 如果本服务名与FeignClient的服务名相同，则不生成代理类
            if (name.equals(getSelfServiceName())) {
                return;
            }
            String feignBeanName = Util.feignlessClientAnnotationName(feignlessClient);
            StaticLog.info("生成Feign服务代理实例，服务名：:{}", feignBeanName);
            SpringFeignlessServiceClient fastFeignClient = clientBuilder.forType(SpringFeignlessServiceClient.class, name)
                    .fallback(SpringFeignlessServiceClientFallBack.class)
                    .url(StrUtil.isBlank(serviceUrl) ? null : serviceUrl)
                    .customize(builder -> {
                        if (feignlessClient.connectTimeout() > 0) {
                            Request.Options options = new Request.Options(feignlessClient.connectTimeout(),
                                    TimeUnit.MILLISECONDS,
                                    feignlessClient.connectTimeout(),
                                    TimeUnit.MILLISECONDS,
                                    true);
                            builder.options(options);
                        }
                        if (feignlessClient.period() > 0) {
                            Retryer retryer = new Retryer.Default(feignlessClient.period(),
                                    feignlessClient.maxPeriod(),
                                    (int) feignlessClient.maxAttempts());
                            builder.retryer(retryer);
                        }
                    })
                    .contextId(feignBeanName).build();
            SpringUtil.registerBean(feignBeanName, fastFeignClient);
        });
        // 生成特定服务的FeignClient代理类
        classes.forEach(clazz -> {
            FeignlessClient feignlessClient = AnnotationUtil.getAnnotation(clazz, FeignlessClient.class);
            // 如果本服务名与FeignClient的服务名相同，则不生成代理类
            if (feignlessClient.serviceName().equals(getSelfServiceName())) {
                return;
            }
            DynamicType.Unloaded<?> dynamicType = new ByteBuddy().subclass(clazz)
                    .name(clazz.getName() + "$PROXY")
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.any())).intercept(FixedValue.nullValue())
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(short.class))).intercept(FixedValue.value((short) 0))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(int.class))).intercept(FixedValue.value(0))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(char.class))).intercept(FixedValue.value('0'))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(byte.class))).intercept(FixedValue.value((byte) 0))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(float.class))).intercept(FixedValue.value(0.0F))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(double.class))).intercept(FixedValue.value(0.0D))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(long.class))).intercept(FixedValue.value(0L))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(boolean.class))).intercept(FixedValue.value(false))
                    .method(ElementMatchers.isDeclaredBy(clazz).and(ElementMatchers.returns(TypeDescription.VOID))).intercept(FixedValue.value(TypeDescription.VOID))
                    //AOP 代理 环绕
                    .method(ElementMatchers.isDeclaredBy(clazz))
                    .intercept(MethodDelegation.to(AopInterceptor.class))
                    .make();
            Class<?> aClass = dynamicType.load(ClassLoaderUtil.getContextClassLoader())
                    .getLoaded();
            try {
                Object newInstance = aClass.getDeclaredConstructor().newInstance();
                SpringUtil.registerBean(feignlessClient.targetBeanName(), newInstance);
                StaticLog.info("生成目标服务为：{}，类为：{} 的服务远程代理Bean", feignlessClient.serviceName(), aClass.getName());
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                     InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        });
        FeignlessRunnableQueue.initSuccess();
        StaticLog.info("FeignlessConfiguration init End");
    }


    private String getSelfServiceName() {
        return SpringUtil.getApplicationName();
    }


    private String[] getBasePackage(AnnotationMetadata metadata) {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(EnableFeignless.class.getName()));
        if (attributes == null) {
            return new String[]{"site.sorghum"};
        }
        return attributes.getStringArray("basePackages");
    }

    private boolean enableFeignless() {
        if (metadata == null) {
            return false;
        }
        AtomicBoolean enable = new AtomicBoolean(false);
        metadata.getAnnotationTypes().forEach(annotationType -> {
            if (annotationType.equals(EnableFeignless.class.getName())) {
                enable.set(true);
            }
        });
        return enable.get();
    }
}
