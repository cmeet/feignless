package site.sorghum.feignless.solon.configuration;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.FIFOCache;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import site.sorghum.feignless.configuration.FeignlessConfigurationProperties;
import site.sorghum.feignless.utils.ContextUtil;
import site.sorghum.feignless.utils.Util;

/**
 * feignless bean 配置
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@Configuration
public class FeignlessBeanConf {

    @Bean
    FIFOCache<String ,String> feignlessCache(){
        return CacheUtil.newFIFOCache(3000);
    }

    @Bean
    ContextUtil contextUtil(){
        return new ContextUtil() {
            @Override
            public <T> T getBean(String name) {
                return Solon.context().getBean(name);
            }

            @Override
            public <T> T getBean(Class<T> clazz) {
                return Solon.context().getBean(clazz);
            }

            @Override
            public <T> T getBean(String name, Class<T> clazz) {
                return Solon.context().getBean(name);
            }
        };
    }

    @Bean
    Util util(@Inject("${feignless}") FeignlessConfigurationProperties configurationProperties,
              @Inject FIFOCache<String, String> feignlessFifoCache,
              @Inject ContextUtil contextUtil) {
        return new Util(configurationProperties, feignlessFifoCache, contextUtil);
    }
}
