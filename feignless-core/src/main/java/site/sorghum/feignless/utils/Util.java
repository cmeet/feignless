package site.sorghum.feignless.utils;

import cn.hutool.cache.Cache;
import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.crypto.symmetric.SM4;
import site.sorghum.feignless.annotions.FeignlessClient;
import site.sorghum.feignless.annotions.InjectFeignBean;
import site.sorghum.feignless.configuration.FeignlessConfigurationProperties;

import java.util.Objects;

/**
 * 工具类
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@SuppressWarnings("AlibabaUndefineMagicConstant")
public class Util{

    private static String privateKey;

    private static SM4 sm4;

    private static ThreadLocal<MD5> md5 = ThreadLocal.withInitial(MD5::new);
    private static Cache<String,String> nonceMap;

    private static ContextUtil contextUtil;

    public Util(FeignlessConfigurationProperties feignlessConfigurationProperties, Cache<String, String> feignlessFifoCache,ContextUtil contextUtil) {
        Util.nonceMap = feignlessFifoCache;
        Util.privateKey = feignlessConfigurationProperties.getPrivateKey();
        Util.sm4 = SmUtil.sm4(privateKey.getBytes());
        Util.contextUtil = contextUtil;
    }

    public static String getMd5(String str) {
        return SecureUtil.md5(str);
    }

    public static String encrypt(String data) {
        return sm4.encryptBase64(data);
    }

    public static String decrypt(String data) {
        return sm4.decryptStr(data);
    }

    public static String nonce() {
        return IdUtil.fastSimpleUUID();
    }

    public static Cache<String,String> getNonceMap(){
        return nonceMap;
    }


    public static boolean isInjectClazz(Object bean) {
        if (Objects.isNull(bean)){
            return false;
        }
        Class<?> targetClass = targetClass(bean);
        return ReflectUtil.getFields(targetClass, field -> AnnotationUtil.getAnnotation(field, InjectFeignBean.class) != null).length != 0;
    }

    public static Class<?> targetClass(Object bean){
        Class<?> clazz = bean.getClass();
        Class<?> targetClass;
        if (isCglibProxyClass(clazz)) {
            targetClass = getUserClass(clazz);
        } else {
            targetClass = clazz;
        }
        return targetClass;
    }


    private static boolean isCglibProxyClassName(String className) {
        return (className != null && className.contains("$$"));
    }


    private static boolean isCglibProxyClass(Class<?> clazz) {
        return (clazz != null && isCglibProxyClassName(clazz.getName()));
    }

    private static Class<?> getUserClass(Class<?> clazz) {
        if (clazz.getName().contains("$$")) {
            Class<?> superclass = clazz.getSuperclass();
            if (superclass != null && superclass != Object.class) {
                return superclass;
            }
        }
        return clazz;
    }

    public static <T> T getBean(String name) {
        return contextUtil.getBean(name);
    }

    public static <T> T getBean(Class<T> clazz) {
        return contextUtil.getBean(clazz);
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        return contextUtil.getBean(name, clazz);
    }

    public static String feignlessClientAnnotationName(FeignlessClient client){
        String string = client.serviceName() +
                client.solonGroup() +
                client.targetBeanName() +
                client.targetClassName() +
                client.readTimeout() +
                client.connectTimeout() +
                client.serviceUrl() +
                client.period() +
                client.maxPeriod() +
                client.maxAttempts();
        return md5.get().digestHex(string);
    }
}
