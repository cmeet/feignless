package site.sorghum.feignless.pojo;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import site.sorghum.feignless.utils.Util;

import java.util.Arrays;

/**
 * 远程请求
 *
 * @author Sorghum
 * @since 2022/11/17
 */
@SuppressWarnings("AlibabaUndefineMagicConstant")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RemoteCommunicateData {
    /**
     * id
     */
    String id;
    /**
     * 目标类型
     */
    String targetType;
    /**
     * 目标名称
     */
    String targetName;
    /**
     * 方法
     */
    String method;
    /**
     * 参数
     */
    Object[] args;
    Boolean success = false;
    /**
     * 返回类型
     */
    String returnType;
    /**
     * 返回数据
     */
    Object returnData;
    /**
     * 异常信息
     */
    String throwableMsg;

    /**
     * 签名
     */
    String sign;

    /**
     * 时间戳
     */
    long timestamp;

    public boolean checkSign() {
        return this.sign.equals(generateSign());
    }

    public String generateSign() {
        return generateSign(false);
    }

    public String generateSign(boolean resetSign) {
        if (timestamp == 0) {
            timestamp = System.currentTimeMillis();
        }
        String newSign = Util.getMd5(Util.encrypt(toString()));
        if (resetSign) {
            this.sign = newSign;
        }
        return newSign;
    }

    public static RemoteCommunicateData EMPTY = new RemoteCommunicateData() {{
        success = false;
    }};

    public static void checkIllegal(RemoteCommunicateData data) {
        if (data == null) {
            throw new IllegalArgumentException("RemoteCommunicateData is null.");
        }
        if (StrUtil.isBlank(data.getMethod())) {
            throw new IllegalArgumentException("RemoteCommunicateData's method is blank.");
        }
        if (StrUtil.isBlank(data.getTargetType())) {
            throw new IllegalArgumentException("RemoteCommunicateData's targetType is blank.");
        }
        if (data.getId() == null) {
            throw new IllegalArgumentException("RemoteCommunicateData's id is null.");
        }
        // 校验时间戳
        if (Math.abs(System.currentTimeMillis() - data.getTimestamp()) > 1000 * 60 * 5) {
            throw new IllegalArgumentException("RemoteCommunicateData's timestamp is illegal.");
        }
        // 校验nonce
        if (Util.getNonceMap().containsKey(data.getId())) {
            throw new IllegalArgumentException("RemoteCommunicateData's nonce is illegal.");
        }
        Util.getNonceMap().put(data.getId(), data.getId(), 1000 * 60 * 5);
        if (!data.checkSign()) {
            throw new IllegalArgumentException("RemoteCommunicateData's sign is illegal.");
        }
    }

    @Override
    public String toString() {
        return this.id + " " + this.targetType + " " + this.targetName + " " + this.method + " " + Arrays.toString(this.args) + " " + this.timestamp;
    }
}