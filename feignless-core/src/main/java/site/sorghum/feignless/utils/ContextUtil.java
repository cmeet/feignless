package site.sorghum.feignless.utils;

/**
 * 上下文工具，用于适配不同框架获取Bean
 *
 * @author Sorghum
 * @since 2023/01/10
 */
public interface ContextUtil {
    /**
     * 通过name获取 Bean
     *
     * @param <T>  Bean类型
     * @param name Bean名称
     * @return Bean
     */
    public <T> T getBean(String name);

    /**
     * 通过class获取Bean
     *
     * @param <T>   Bean类型
     * @param clazz Bean类
     * @return Bean对象
     */
    public <T> T getBean(Class<T> clazz);

    /**
     * 通过name,以及Clazz返回指定的Bean
     *
     * @param <T>   bean类型
     * @param name  Bean名称
     * @param clazz bean类型
     * @return Bean对象
     */
    public <T> T getBean(String name, Class<T> clazz);
}
