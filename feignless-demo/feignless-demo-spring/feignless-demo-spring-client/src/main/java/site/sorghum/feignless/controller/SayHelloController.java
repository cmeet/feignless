package site.sorghum.feignless.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.sorghum.feignless.annotions.InjectFeignBean;
import site.sorghum.feignless.service.DemoService;

/**
 * 说hello控制器
 *
 * @author Sorghum
 * @since 2023/01/12
 */
@RestController
@RequestMapping("/sayHello")
public class SayHelloController {

    @InjectFeignBean
    DemoService demoService;

    @RequestMapping
    public String sayHello(){
        return demoService.hello("World");
    }
}
