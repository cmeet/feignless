package site.sorghum.feignless.solon.client;

import feign.Headers;
import feign.RequestLine;
import site.sorghum.feignless.client.FeignlessServiceClient;
import site.sorghum.feignless.pojo.RemoteCommunicateData;

/**
 * 快装客户端
 *
 * @author Sorghum
 * @since 2022/12/15
 */
public interface SolonFeignlessServiceClient extends FeignlessServiceClient {

    /**
     * 调用
     *
     * @param remoteCommunicateData 远程通信数据
     * @return {@link RemoteCommunicateData}
     */
    @Override
    @RequestLine(value = "POST /extend/feignless/invoke/")
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    RemoteCommunicateData invoke(RemoteCommunicateData remoteCommunicateData);
}
