package site.sorghum.feignless.spring;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author Sorghum
 */
@ComponentScan("site.sorghum.feignless")
public class AutoConfiguration {
}
