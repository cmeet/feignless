package site.sorghum.feignless.service;

import cn.hutool.log.StaticLog;
import lombok.Setter;

public class DemoFallbackService implements DemoService {
    @Setter
    Throwable cause;

    @Override
    public String hello(String name) {
        StaticLog.error("error:{}", cause);
        return "fallback";
    }

    @Override
    public void sayHello(String name) {
        StaticLog.error("error:{}", cause);
    }
}
