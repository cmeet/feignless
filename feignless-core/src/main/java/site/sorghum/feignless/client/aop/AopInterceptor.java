package site.sorghum.feignless.client.aop;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.log.StaticLog;
import net.bytebuddy.implementation.bind.annotation.AllArguments;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import site.sorghum.feignless.annotions.FeignlessClient;
import site.sorghum.feignless.client.FeignlessServiceClient;
import site.sorghum.feignless.exception.FeignlessException;
import site.sorghum.feignless.pojo.RemoteCommunicateData;
import site.sorghum.feignless.utils.Util;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * aop拦截器
 *
 * @author sorghum
 * @since 2022/11/16
 */
public class AopInterceptor {

    private final static Map<String, FeignlessServiceClient> FAST_FEIGNCLIENT_MAP = new HashMap<>();

    private final static ThreadLocal<Map<String,Object>> FAST_FALLBACK_MAP = ThreadLocal.withInitial(HashMap::new);

    private final static String SET_CAUSE_METHOD = "setCause";
    /**
     * 拦截
     *
     * @param arguments     参数
     * @param method   方法
     * @return {@link Object}
     */
    @RuntimeType
    public static Object handle(@AllArguments Object[] arguments, @Origin Method method){
        Class<?> declaringClass = method.getDeclaringClass();
        FeignlessClient annotation = AnnotationUtil.getAnnotation(declaringClass, site.sorghum.feignless.annotions.FeignlessClient.class);
        if (annotation == null) {
            throw new FeignlessException("FeignlessClient注解不存在");
        }
        RemoteCommunicateData remoteCommunicateData = new RemoteCommunicateData();
        remoteCommunicateData.setId(IdUtil.fastSimpleUUID());
        remoteCommunicateData.setMethod(method.getName());
        remoteCommunicateData.setArgs(arguments);
        remoteCommunicateData.setTargetType(StrUtil.isBlank(annotation.targetClassName()) ? declaringClass.getName() : annotation.targetClassName());
        remoteCommunicateData.setReturnType(method.getReturnType().getName());
        remoteCommunicateData.setTargetName(annotation.targetBeanName());
        remoteCommunicateData.generateSign(true);
        FeignlessServiceClient feignlessServiceClient = initFastFeignClient(Util.feignlessClientAnnotationName(annotation));
        RemoteCommunicateData invoke = remoteCommunicateData;
        // ----------------- 降级 -----------------
        try {
            invoke = feignlessServiceClient.invoke(remoteCommunicateData);
        }catch (Exception e){
            remoteCommunicateData.setThrowableMsg(ExceptionUtil.getRootCauseMessage(e));
        }
        // --- 接收服务 ----
        if (invoke.getSuccess()){
            // --- 服务调用成功 直接返回 ---
            return invoke.getReturnData();
        }
        // --- 服务调用失败 ----
        Class<?> fallback = annotation.fallback();
        // --- 是否降级服务 ----
        if (fallback.equals(void.class)){
            // --- 无降级服务 ----
            if (invoke.getThrowableMsg() != null){
                throw FeignlessException.of(invoke.getThrowableMsg());
            }else {
                throw new FeignlessException("未知异常");
            }
        }else {
            // --- 有降级服务 ----
            Object fallBackInstance = getFallback(fallback);
                Method causeMethod;

            if (invoke.getThrowableMsg() == null){
                invoke.setThrowableMsg("未知异常");
            }

            if ((causeMethod = ReflectUtil.getMethodByName(fallback,SET_CAUSE_METHOD)) != null){
                // 有setCause方法
                ReflectUtil.invoke(fallBackInstance,causeMethod,FeignlessException.of(invoke.getThrowableMsg()));
            }else {
                // 无setCause方法
                StaticLog.error("远程调用异常,{}",invoke.getThrowableMsg());
            }
            return ReflectUtil.invoke(fallBackInstance,method.getName(),arguments);
        }
    }

    private static FeignlessServiceClient initFastFeignClient(String serviceName){
        if (!FAST_FEIGNCLIENT_MAP.containsKey(serviceName)){
            FAST_FEIGNCLIENT_MAP.put(serviceName, Util.getBean(serviceName));
        }
        return FAST_FEIGNCLIENT_MAP.get(serviceName);
    }

    private static <FB> FB getFallback(Class<FB> fallback){
        if (!FAST_FALLBACK_MAP.get().containsKey(fallback.getName())){
            FAST_FALLBACK_MAP.get().put(fallback.getName(), ReflectUtil.newInstance(fallback));
        }
        return (FB) FAST_FALLBACK_MAP.get().get(fallback.getName());
    }
}