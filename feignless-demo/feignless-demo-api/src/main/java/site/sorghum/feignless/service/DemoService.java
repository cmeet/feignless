package site.sorghum.feignless.service;

import site.sorghum.feignless.annotions.FeignlessClient;

/**
 * 演示服务
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@FeignlessClient(serviceName = "demo-server", solonGroup = "demo",targetBeanName = "demoService",readTimeout = 1000,connectTimeout = 1000,fallback = DemoFallbackService.class)
public interface DemoService {

    /**
     * 你好
     *
     * @param name 名字
     * @return {@link String}
     */
    String hello(String name);

    /**
     * 说“你好”
     *
     * @param name 名字
     */
    void sayHello(String name);

}
