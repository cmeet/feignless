package site.sorghum.feignless.client;

import site.sorghum.feignless.pojo.RemoteCommunicateData;

/**
 * 快装客户端
 *
 * @author Sorghum
 * @since 2022/12/15
 */
public interface FeignlessServiceClient {

    /**
     * 调用
     *
     * @param remoteCommunicateData 远程通信数据
     * @return {@link RemoteCommunicateData}
     */
    RemoteCommunicateData invoke(RemoteCommunicateData remoteCommunicateData);
}
