package site.sorghum.feignless.annotions;

import java.lang.annotation.*;

/**
 * @author Sorghum
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface InjectFeignBean {
    /**
     * Hook的Bean名称
     *
     * @return {@link String}
     */
    String targetBeanName()  default "";

    /**
     * Hook的Bean类型
     *
     * @return {@link Class}<{@link ?}>
     */
    Class<?> targetBeanType() default Object.class;
}
