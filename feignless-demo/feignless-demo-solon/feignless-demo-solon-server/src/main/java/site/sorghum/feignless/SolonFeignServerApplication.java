package site.sorghum.feignless;

import org.noear.solon.Solon;
import site.sorghum.feignless.solon.annotions.EnableFeignless;

/**
 * 装服务器应用程序
 *
 * @author Sorghum
 * @since 2022/12/15
 */
@EnableFeignless(basePackages = "site.sorghum")
public class SolonFeignServerApplication {
    public static void main(String[] args) {
        Solon.start(SolonFeignServerApplication.class, args);
    }
}