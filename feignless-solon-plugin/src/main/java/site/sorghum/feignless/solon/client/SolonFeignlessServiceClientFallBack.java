package site.sorghum.feignless.solon.client;

import site.sorghum.feignless.pojo.RemoteCommunicateData;

/**
 * 快装客户端回调
 *
 * @author Sorghum
 * @since 2022/12/15
 */
public class SolonFeignlessServiceClientFallBack implements SolonFeignlessServiceClient {
    @Override
    public RemoteCommunicateData invoke(RemoteCommunicateData remoteCommunicateData) {
        return RemoteCommunicateData.EMPTY;
    }
}
