package site.sorghum.feignless.exception;

/**
 * feignless异常
 *
 * @author Sorghum
 * @since 2023/02/03
 */
public class FeignlessException extends RuntimeException{

    public FeignlessException(String message) {
        super(message);
    }

    public static FeignlessException of(String message) {
        return new FeignlessException(message);
    }

    public static FeignlessException of(String message, Throwable cause) {
        return new FeignlessException(message, cause);
    }

    public FeignlessException(String message, Throwable cause) {
        super(message, cause);
    }

    public FeignlessException(Throwable cause) {
        super(cause);
    }

}
