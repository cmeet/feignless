package site.sorghum.feignless.spring.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import site.sorghum.feignless.configuration.FeignlessConfigurationProperties;

/**
 * feignless配置属性
 *
 * @author Sorghum
 * @since 2022/12/16
 */
@ConfigurationProperties(prefix = "feignless")
@Configuration
public class SpringFeignlessConfigurationProperties extends FeignlessConfigurationProperties {
}
