package site.sorghum.feignless.spring.configuration;

import org.springframework.context.annotation.Configuration;

import java.util.LinkedList;

/**
 * @author Sorghum
 */
@Configuration
public class FeignlessRunnableQueue {
    static volatile boolean isInit = false;
    static LinkedList<Runnable> queue = new LinkedList<Runnable>();

    public synchronized static void addRunnable(Runnable runnable){
        if (isInit){
            runnable.run();
        }
        queue.add(runnable);
    }

    public synchronized static void initSuccess() {
        isInit = true;
        queue.forEach(Runnable::run);
    }
}
