package site.sorghum.feignless.server;

import cn.hutool.core.date.StopWatch;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.StaticLog;
import site.sorghum.feignless.pojo.RemoteCommunicateData;
import site.sorghum.feignless.utils.Util;

import java.util.concurrent.TimeUnit;

/**
 * 快装控制器
 *
 * @author Sorghum
 * @since 2022/12/15
 */

public class FeignlessController {

    public RemoteCommunicateData invoke(RemoteCommunicateData remoteCommunicateData) {
        StopWatch stopWatch = new StopWatch(remoteCommunicateData.getId());
        try {
            StaticLog.info("feignless invoke: {}", JSONUtil.toJsonStr(remoteCommunicateData));
            stopWatch.start("Check Param & Sign");
            RemoteCommunicateData.checkIllegal(remoteCommunicateData);
            stopWatch.stop();
            stopWatch.start("Init Env");
            ClassLoader contextClassLoader = ClassUtil.getContextClassLoader();
            Class<?> aClass = contextClassLoader.loadClass(remoteCommunicateData.getTargetType());
            String beanName = remoteCommunicateData.getTargetName();
            Object bean;
            if (StrUtil.isBlank(beanName)) {
                bean = Util.getBean(aClass);
            } else {
                bean = Util.getBean(beanName);
            }
            stopWatch.stop();
            stopWatch.start("Execute Method");
            Object invoke = ReflectUtil.invoke(bean, remoteCommunicateData.getMethod(), remoteCommunicateData.getArgs());
            stopWatch.stop();
            remoteCommunicateData.setReturnData(invoke);
            remoteCommunicateData.setSuccess(true);
            StaticLog.info("\n{}",stopWatch.prettyPrint(TimeUnit.MILLISECONDS));
        } catch (Exception e) {
            StackTraceElement rootStackElement = getSelfStackTraceElement(e);
            // 打印所有详细信息
            String message = ExceptionUtil.getRootCauseMessage(e);
            int lineNumber = rootStackElement.getLineNumber();
            String fileName = rootStackElement.getFileName();
            String className = rootStackElement.getClassName();
            String methodName = rootStackElement.getMethodName();
            String errorMsg = StrUtil.format("{} at {}.{}({}:{})", message, className, methodName, fileName, lineNumber);
            remoteCommunicateData.setThrowableMsg(errorMsg);
            remoteCommunicateData.setSuccess(false);
            StaticLog.error(errorMsg);
        }
        return remoteCommunicateData;
    }

    private static StackTraceElement getSelfStackTraceElement(Exception e) {
        // 找到根异常
        Throwable rootCause = ExceptionUtil.getRootCause(e);
        // 找到本包下的异常
        StackTraceElement[] stackTrace = rootCause.getStackTrace();
        return stackTrace[0];
    }
}
